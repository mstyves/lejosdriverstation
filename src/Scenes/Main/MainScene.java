package Scenes.Main;

import Views.Interface;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainScene extends Scene {

	private Button btnNxtConnect = new Button ("Connect");
	private Button btnManetteConnect = new Button ("Connect");
	private Button btnManetteSend = new Button ("Send");
	
	private Label lblManetteConnect = new Label("Manette: ");
	private Label lblNxtConnect = new Label("NXT: ");
	private Label lblNotifications = new Label("");

	public MainScene() {
		super(new Group(), 450, 400);
		
		GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        // Ligne 0
        grid.add(lblNotifications, 0, 0);
        // Ligne 1
        grid.add(lblNxtConnect, 0, 1);
        grid.add(btnNxtConnect, 1, 1);
        btnNxtConnect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            	ButtonNxtConnect btn = (ButtonNxtConnect)e.getSource();
            	//Interface.primaryStage.get
            }
        });
        // Ligne 2
        grid.add(lblManetteConnect, 0, 2);
		grid.add(btnManetteConnect, 1, 2);
		grid.add(btnManetteSend, 2, 2);
        // Ligne 3
        
        
        btnManetteSend.setDisable(true);
        
        Group root = (Group)this.getRoot();
        root.getChildren().add(grid);
		// TODO Auto-generated constructor stub
	}
	
}
