package Views;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class AccueilScenes {
	final static Button btnConnect = new Button ("Connect");
	final static Button btnManetteConnect = new Button ("Connect");
	final static Button btnSend = new Button ("Send data!");
	final static Button btnStopSend = new Button ("Stop sending data!");
	
	final static Label lblManetteConnect = new Label("Manette non-connect�");
	final static Label lblNxtConnect = new Label("NXT non-connect�");


	
	public static Scene getMainScene() {
		 AccueilScenes.btnSend.setDisable(true);
		
		//layout
		Scene scene = new Scene(new Group(), 450, 400);
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        // Ligne 1
        grid.add(lblNxtConnect, 1, 0);
        // Ligne 2
        grid.add(btnConnect, 0, 1);

        // Ligne 3
        grid.add(lblManetteConnect, 1, 3);
        grid.add(btnManetteConnect, 0, 4);
        grid.add(btnSend, 1, 4);

        Group root = (Group)scene.getRoot();
        root.getChildren().add(grid);
		
		// Set de l'affichage
		return scene;
	}
}
