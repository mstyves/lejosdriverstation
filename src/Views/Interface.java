package Views;


import java.io.IOException;

import Views.connections.mainWindow;
import driver.Communication;
import driver.Manette;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
	
public class Interface extends Application {
	static Manette manettes;
	static Communication comm;
	static Task<Integer> sendTask;
	public static Stage primaryStage;
	Stage accueil = new Stage();
	
	@Override
	public void start(Stage primaryStage) throws InterruptedException {
	
		Interface.primaryStage = primaryStage;
		primaryStage.setTitle("DriverStation (Mathieu)");
		
		
		primaryStage.setScene(AccueilScenes.getMainScene());
		
		primaryStage.show();
		
		//setBtnActions();
	}

	public static void main(String[] args) {
		launch(args);
	}
	/*
	private void connectManette(){
		 manettes = new Manette();
		 System.out.println(manettes.numberOfManetteConnected());
		 if(manettes.isAnyManetteConnected()){
			 AccueilScenes.lblManetteConnect.setText("Manette connect�!");
			 AccueilScenes.btnManetteConnect.setText("Disconnect!");
			 
			 if(comm != null && comm.isAnyNxtConnected())
			 {
				 btnSend.setDisable(false);
				 btnSend.setText("Send data!");
			}
			 
		 }
	}
	
	private void disconnectManette(){
		 if(manettes.isAnyManetteConnected()){
			 manettes.disconnect();
			 AccueilScenes.lblManetteConnect.setText("Manette non-connect�");
			 AccueilScenes.btnManetteConnect.setText("Connect");
		 }
	}
	
	private void stopComm() throws IOException{
		if(sendTask!=null){ sendTask.cancel(); }
		comm.disconnect();
	}
	
	private void startComm(){
		 comm = new Communication();
		 comm.connect();
		 
		 if(comm.isAnyNxtConnected()){
			AccueilScenes.lblNxtConnect.setText("NXT Connect�!");
			AccueilScenes.btnConnect.setText("Disconnect");
			if(manettes != null && manettes.isAnyManetteConnected())
			{
				AccueilScenes.btnSend.setDisable(false);
				AccueilScenes.btnSend.setText("Send data!");
			}else{
				AccueilScenes.btnSend.setDisable(true);
				AccueilScenes.btnSend.setText("Send data!");
			}
		 }else{
			 AccueilScenes.lblNxtConnect.setText("NXT nont-connect�");
			 AccueilScenes.btnConnect.setText("Connect");
			 AccueilScenes.btnSend.setDisable(true);
			 AccueilScenes.btnSend.setText("Send data!");
		 }
	}
	
	private void startSending(){
		sendTask = new Task<Integer>() {
		    @Override protected Integer call() throws Exception {
				System.out.println("Connect�!");
				String enco;
			while(!isCancelled())
				{
					enco = manettes.encode();
					try {
						comm.sendNXT(enco);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						System.out.println("Une erreur s'est produite lors de l'envoi des donn�es.");
						// Sur echec on ferme le thread
						this.cancel();
						e.printStackTrace();
					}
					// 1 si le boutons est appuyer
					//btn_sortie = (int) (new JSONArray(new JSONObject(enco).get("B").toString()).get(7));
					// L'envoi doit etre plus lent que la reception
					Thread.sleep(50);
				}
				
				System.out.println("Fin du programme");
		      return 1;
		    }
		};
		new Thread(sendTask).start();
	}
	
	private void setBtnActions(){
		// Link des evenements boutons...
				AccueilScenes.btnConnect.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	if(AccueilScenes.btnConnect.getText() == "Connect"){
		            		startComm();
		            	}else{
		            		try {
								stopComm();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
		            	}
		            }
		        });
				
				AccueilScenes.btnSend.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	startSending();
		            }
		        });
				
				AccueilScenes.btnStopSend.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	sendTask.cancel();
		            }
		        });
				
				AccueilScenes.btnManetteConnect.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	if(AccueilScenes.btnManetteConnect.getText() == "Connect"){
		            		connectManette();
		            	}else{
		            		disconnectManette();
		            	}	
		            }
		        });
	}*/
}
