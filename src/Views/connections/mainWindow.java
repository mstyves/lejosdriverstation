package Views.connections;
/*
 * Classe fonctionnel PC
 * 
 * 
 */
import java.io.IOException;

import driver.Communication;
import driver.Manette;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class mainWindow extends Application {

	private Manette manettes = new Manette();
	private Communication comm = new Communication();
	
	// Thread qui gere les communication avec le NXT
	private Task<Integer> sendTask = new Task<Integer>() {
	    @Override protected Integer call() throws Exception {
			System.out.println("Connect�!");
			String enco;
			//System.out.println("rc:" + comm.waitConfirm());
		while(!isCancelled())
			{
				enco = manettes.encode();
				try {
					comm.sendNXT(enco);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Une erreur s'est produite lors de l'envoi des donn�es.");
					this.cancel();
					e.printStackTrace();
				}
				// 1 si le boutons est appuyer
				//btn_sortie = (int) (new JSONArray(new JSONObject(enco).get("B").toString()).get(7));
				// L'envoi doit etre plus lent que la reception
				System.out.println("Debut attente confirm");
				System.out.println("rc:" + comm.waitConfirm());
//				System.out.println("Confirmation recu!");
			}
			
			System.out.println("Fin de l'envoi");
	      return 1;
	    }
	};
	
	private Button btnNxtConnect = new Button ("Connect");
	private Button btnManetteConnect = new Button ("Connect");
	private Button btnManetteSend = new Button ("Send");
	
	private Label lblManetteConnect = new Label("Manette: ");
	private Label lblNxtConnect = new Label("NXT: ");
	private Label lblNotifications = new Label("");
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("DriverStation V2 (Mathieu)");
		
		
		//layout
		Scene scene = new Scene(new Group(), 450, 400);
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        // Ligne 0
        grid.add(lblNotifications, 0, 0);
        // Ligne 1
        grid.add(lblNxtConnect, 0, 1);
        grid.add(btnNxtConnect, 1, 1);
        // Ligne 2
        grid.add(lblManetteConnect, 0, 2);
		grid.add(btnManetteConnect, 1, 2);
		grid.add(btnManetteSend, 2, 2);
        // Ligne 3
        
        
        btnManetteSend.setDisable(true);
        
        Group root = (Group)scene.getRoot();
        root.getChildren().add(grid);
		

		primaryStage.setScene(scene);
		
		primaryStage.show();
		setBtnActions();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private boolean startCommunication(){

		if(comm.pairing() && comm.connect())
		{
			return true;
		}else
		{
			lblNotifications.setText("Une erreur s'est produite.");
			return false;
		}
	}
	
	private boolean stopCommunication(){
		this.sendTask.cancel();
		return comm.disconnect();
	}
	
	private boolean connectManettes(){
		return this.manettes.connect();
	}
	
	private boolean disconnectManettes(){
		return this.manettes.disconnect();
	}
	
	private void startSending(){
		new Thread(sendTask).start();
	}
	
	private void setBtnActions(){
		// Link des evenements boutons...
				btnNxtConnect.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		        		lblNotifications.setText("Connection...");
		            	//System.out.println("TATA");
		            	if(btnNxtConnect.getText() == "Connect" && startCommunication()){
		            		// Gestion de laffichage
		            		btnNxtConnect.setText("Disconnect");
		            		lblNotifications.setText("Connected!");
		            		if(manettes.isAnyManetteConnected())
		            			btnManetteSend.setDisable(false);
		            	}else{
		            		if(stopCommunication()){
		            			btnNxtConnect.setText("Connect");
		            			lblNotifications.setText("Disconnected!");
		            			btnManetteSend.setDisable(true);
		            		}
		            	}
		            }
		        });
				
				btnManetteSend.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	if(btnManetteSend.getText() == "Send")
		            	{
		            		startSending();
		            		btnManetteSend.setText("Stop sending!");
		            	}else{
		            		sendTask.cancel();
		            		btnManetteSend.setText("Send");
		            	}
		            }
		        });
				
				btnManetteConnect.setOnAction(new EventHandler<ActionEvent>() {
		            @Override
		            public void handle(ActionEvent e) {
		            	if(btnManetteConnect.getText() == "Connect" && connectManettes()){
		            		//Affichage
		            		btnManetteConnect.setText("Disconnect!");
		            		if(comm.isAnyNxtConnected())
		            			btnManetteSend.setDisable(false);
		            		else
		            			btnManetteSend.setDisable(true);
		            	}else{
		            		if(disconnectManettes()){
		            			btnManetteConnect.setText("Connect");
		            			btnManetteSend.setDisable(true);
		            		}
		           
		            	}	
		            }
		        });
	}
	
}
