package console;

import java.io.IOException;

import driver.Communication;
import driver.Manette;
import json.*;

public class DriverStation {
	static Manette manettes = new Manette();
	static Communication comm = new Communication();
	
	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		manettes.connect();
		comm.pairing();
		comm.connect();
		
		int btn_sortie = 0;
		System.out.println("Connect�!");
		String enco;

		while(btn_sortie == 0)
		{
			enco = manettes.encode();
			System.out.println("debut envoit");
			comm.sendNXT(enco);
			System.out.println("Fin envoit");
			System.out.println("debut Confirm");
			
			System.out.println("Fin confirm");
			// 1 si le boutons est appuyer
			btn_sortie = (int) (new JSONArray(new JSONObject(enco).get("B").toString()).get(7));
			// L'envoi doit etre plus lent que la reception
			//Thread.sleep(50);
		}
		
		System.out.println("Fin du programme");
	}

}
