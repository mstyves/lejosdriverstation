package driver;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTCommLogListener;
import lejos.pc.comm.NXTConnector;
import lejos.pc.comm.NXTInfo;

public class Communication {
	private NXTComm nxtComm;
    private NXTInfo[] nxts;
    private NXTConnector conn = new NXTConnector();
    private boolean open = false;
	private DataOutputStream outDat;
	private DataInputStream inDat;
	private int protocol = NXTCommFactory.BLUETOOTH;
			
	public Communication(){
	}
	
	public boolean pairing(){
		conn.addLogListener(new NXTCommLogListener(){
			public void logEvent(String message) {
				System.out.println("USBSend Log.listener: "+message);
				
			}

			public void logEvent(Throwable throwable) {
				System.out.println("USBSend Log.listener - stack trace: ");
				 throwable.printStackTrace();
			}
		});
		
		System.out.println("Searching...");         
	    System.out.println("Initiated!");
	    nxts = conn.search(null, null, protocol);
	    
	    return nxts.length > 0;
	}
	
	public boolean connect(){
	       try{
	         if (nxts.length == 0) {
	            System.out.println("No NXTS to connect!");
	            return false;
	         }
	         nxtComm = NXTCommFactory.createNXTComm(protocol);
	      
	         open = nxtComm.open(nxts[nxts.length - 1], NXTComm.LCP);
	         outDat = new DataOutputStream(nxtComm.getOutputStream());
	         inDat = new DataInputStream(nxtComm.getInputStream());
	         System.out.println("Connection open!");
	         System.out.println("Connected: " + open);
	         return true;
	         
	       } catch (NXTCommException e) {
	    	 System.out.println(e);
	         System.err.println("NXTCommException caught!");
	         return false;
	      }
	}
	
	public boolean disconnect(){
		try {
			nxtComm.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public boolean isAnyNxtConnected(){
		return nxts.length > 0;
	}
	public void sendNXT(String data) throws IOException{
		
		// Valider si on peut ne pas le reinstancier a chaque fois,
		// probalement une perte de performance ici
		
		
		outDat.writeUTF(data+"\n");
		outDat.flush();
		//outDat.close();
		
	}
	
	public String waitConfirm() throws IOException{
		while(!inDat.readUTF().contains("SendNext")){}
		//String str = inDat.readUTF();
		//System.out.println("recu: " + str);
		return "SendNext";
	}
}
